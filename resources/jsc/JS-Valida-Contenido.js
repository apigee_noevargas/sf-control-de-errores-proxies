 try {
    JSON.parse(context.getVariable('response.content'));
} catch (err) {
    context.setVariable("flow.isValidJson", false);
    print(err.message);
}